package com.yBayApplication.customerAccount;

import org.springframework.stereotype.Component;

@Component
public interface CustomerRepository{
	public Customer getCustomerByUserName(String userName);
}
