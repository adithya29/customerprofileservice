package com.yBayApplication.customerAccount;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;

@Controller
@EnableWebMvc
@RequestMapping("/ybay")
public class CustomerController /*extends WebMvcConfigurerAdapter */{

	private static final Logger log = Logger.getLogger(CustomerController.class.getName());
	@Autowired
	CustomerRepository custRepo;
	
	
	/*@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/ybay/profileManager").setViewName("profileManager");
        registry.addViewController("/ybay/login").setViewName("login");
        registry.addViewController("/ybay/profileNotFound/{userName}").setViewName("profileNotFound");
    }
	
	
	@Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/templates/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
*/	
	@Bean
	public SpringResourceTemplateResolver templateResolver(){
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setPrefix("/WEB-INF/templates/");
		resolver.setSuffix(".jsp");
		resolver.setCacheable(false);
		return resolver;
		
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(){
		return "login";
	}
	
	
	
	
	@RequestMapping(value ="/profileManager")
	public String getProfileManager(@RequestParam("userName") String userName,ModelMap map){
		//String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		if(userName == null){
			userName = "jack";
			log.log(Level.INFO, "username is not acquired by spring security");
		}
		log.log(Level.INFO, "fetching customer information");
		map.addAttribute("customer",custRepo.getCustomerByUserName(userName));
		return "profileManager";
	}
	
	@RequestMapping(value="/profileNotFound")
	public String profileNotfound(@RequestParam("userName") String userName,ModelMap map){
		userName = SecurityContextHolder.getContext().getAuthentication().getName();
		map.addAttribute("userName",userName);
		return "profileNotFound";
	}

}
