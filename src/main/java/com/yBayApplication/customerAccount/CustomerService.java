package com.yBayApplication.customerAccount;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustomerService implements CustomerRepository{

	@Autowired
	RestTemplate restTemplate;
	
	private static final Logger log = Logger.getLogger(CustomerService.class.getName());
	
	@Override
	public Customer getCustomerByUserName(String userName) {
		log.log(Level.INFO, "Getting customer details by logged in Username '" +userName+ "'");
		Customer cust = restTemplate.getForObject("http://localhost:8090/ybay/profileManager/{userName}",Customer.class,userName);
		log.log(Level.INFO, "Customer email: " +cust.getEmail()+ " ,Customer ID: " +cust.getId());
		return cust;
	}

	
}
