package com.yBayApplication.customerAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomerDetailsServiceImpl implements UserDetailsService{

	@Autowired
	CustomerRepository custRepo;
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Customer cust = custRepo.getCustomerByUserName(userName);
		return new org.springframework.security.core.userdetails.User(cust.getUserName(), cust.getPasswd(),null);
	}

}
