<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
    <head>
        <title>Welcome to Ybay.com </title>
    </head>
    <body>
        <div th:if="${param.error}">
            <b><font face ="Times New Roman" color = "red">Invalid username and password.</font></b>
        </div>
        <div th:if="${param.logout}">
            <b><font face ="Times New Roman" color = "red">You have been logged out.</font></b>
        </div>
        <div align = "center">
        <form th:action="login" method="post">
            <div><label><b> User Name 	:</b> <input type="text" name="username"/> </label></div>
            <div><label><b> Password	 :</b> <input type="password" name="password"/> </label></div>
            <div><input type="submit" value="Sign In"/></div>
        </form>
        </div>
    </body>
</html>